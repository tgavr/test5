// Generated by CoffeeScript 1.7.1
var App, checkSettings, form, on400, on409, onError, onSuccess, sendSettings,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

App = {
  Modules: {}
};

App.Modules.Alert = new ((function() {
  function _Class() {
    this._showAlert = __bind(this._showAlert, this);
    this.errorAlert = __bind(this.errorAlert, this);
    this.warningAlert = __bind(this.warningAlert, this);
    this.successAlert = __bind(this.successAlert, this);
    this._alert = $("#app_alert");
  }

  _Class.prototype.successAlert = function(text) {
    return this._showAlert('success', text);
  };

  _Class.prototype.warningAlert = function(text) {
    return this._showAlert('warning', text);
  };

  _Class.prototype.errorAlert = function(text) {
    return this._showAlert('danger', text);
  };

  _Class.prototype._showAlert = function(cssClass, text) {
    var defferedHide, hide;
    hide = (function(_this) {
      return function() {
        return _this._alert.fadeOut(700);
      };
    })(this);
    defferedHide = function() {
      return setTimeout(hide, 500);
    };
    this._alert.html(text).attr('class', "alert alert-" + cssClass).fadeIn(700, defferedHide);
    return this;
  };

  return _Class;

})());

var showMessage = function (resp, status) {
  if (status === "success") {
    App.Modules.Alert.successAlert(resp);
  } else {
    App.Modules.Alert.errorAlert(resp.responseText);
  }
}