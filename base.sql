CREATE TABLE `account` (
  `name` VARCHAR(45) NOT NULL,
  `serial` VARCHAR(45) NOT NULL,
  `balance` DECIMAL(65,2) NOT NULL,
  PRIMARY KEY (`serial`));

INSERT INTO `account` (`name`, `serial`) VALUES ('system', '0');

CREATE TABLE `transfer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sender` VARCHAR(45) NOT NULL,
  `recepient` VARCHAR(45) NOT NULL,
  `sum` DECIMAL(65,2) NOT NULL,
  `sbalance` DECIMAL(65,2) NOT NULL,
  `rbalance` DECIMAL(65,2) NOT NULL,
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));

