<?php

class Transfer extends CI_Model {

	const TAX = 0.0099;

	const SYSTEM_ACCOUNT = "0";

	public function makeTransfer($sender, $recepient, $sum) 
	{
		$transfer = array(
			'sender' => $sender,
			'recepient' => $recepient,
			'sum' => $sum
		);

		$tax = $this->_getTax($sum);

		$this->load->model('Account');

		$this->db->trans_start();
		$this->Account->debit($sender, $sum + $tax);

		if (!$this->_dataChanged()) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->Account->credit($recepient, $sum);
			$this->Account->credit(self::SYSTEM_ACCOUNT, $tax);
			$this->db->trans_complete();

			$transfer += $this->Account->getSenderRecepientBalance($sender, $recepient);
			$this->db->insert('transfer', $transfer);

			return true;
		}		
	}

	public function getTransfersBySenderSerial($serial)
	{
		$this->db->where('sender', $serial);
		$this->db->or_where('recepient', $serial);
		$result = $this->db->get('transfer')->result();

		foreach ($result as $transfer) {
			$transfer = ($transfer->sender === $serial) 
				? $this->_converTransferForSender($transfer) 
				: $this->_converTransferForRecepient($transfer);
		}

		return $result;
	}

	private function _getTax($sum) {
		return round(self::TAX * $sum, 2);
	}

	private function _converTransferForSender($transfer) 
	{
		$transfer->sum += $this->_getTax($transfer->sum);
		$transfer->sum *= -1;
		$transfer->balance = $transfer->sbalance;

		return $transfer;
	}

	private function _converTransferForRecepient($transfer)
	{
		$transfer->balance = $transfer->rbalance;

		return $transfer;
	}

	private function _dataChanged()
	{
		return $this->db->affected_rows() !== 0;
	}
}
