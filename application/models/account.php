<?php

class Account extends CI_Model {

	const MYSQL_DUPLICATE_PRIMARY_KEY = 1062;

	public function addAccount($account) 
	{
		$this->db->insert('account', $account);
	}

	public function getAccounts()
	{
		return $this->db->get('account')->result();
	}

	public function debit($serial, $sum)
	{
		$this->db->where('serial', $serial);
		$this->db->where('balance >', $sum);
		$this->db->set('balance', "balance - $sum", false);
		$this->db->update('account');
	}

	public function credit($serial, $sum)
	{
		$this->db->where('serial', $serial);
		$this->db->set('balance', "balance + $sum", false);
		$this->db->update('account');
	}

	public function getSenderRecepientBalance($sender, $recepient)
	{
		$this->db->where('serial', $sender);
		$this->db->or_where('serial', $recepient);
		
		$queryResult = $this->db->get('account')->result();

		if ($queryResult[0]->serial == $sender) {
			$result = array(
				'sbalance' => $queryResult[0]->balance, 
				'rbalance' => $queryResult[1]->balance
			);
		} else {
			$result = array(
				'rbalance' => $queryResult[0]->balance, 
				'sbalance' => $queryResult[1]->balance
			);
		}

		return $result;
	}

	public function wasSerialConflict()
	{
		return $this->db->_error_number() == self::MYSQL_DUPLICATE_PRIMARY_KEY;
	}
}
