<?php if (empty($transfers)) { ?> 
	<div class="centered">
		<h1>Нет ни одного перевода</h1>
		<a href="/accounts">Назад</a>
	</div>
<?php } else { ?>
<table class="centered table table-striped">
	<tr>
		<th>Отправитель</th>
		<th>Получатель</th>
		<th>Расход/Приход</th>
		<th>Остаток</th>
		<th>Дата</th>
	</tr>
	<?php foreach ($transfers as $transfer) { ?>
	<tr>
		<td><?= $transfer->sender ?></td>
		<td><?= $transfer->recepient ?></td>
		<td style="color:<?= $transfer->sum < 0 ? 'red' : 'green' ?>">
			<?= $transfer->sum ?>
		</td>
		<td><?= $transfer->balance ?></td>
		<td><?= $transfer->date ?></td>
	</tr>
	<?php } ?>
</table>

<?php } ?>
