<table class="centered table table-striped">
	<tr>
		<th>Имя владельца</th>
		<th>Серийный номер</th>
		<th>Баланс</th>
	</tr>


	<?php foreach ($accounts as $account) { ?>
	<tr>
		<td><?= $account->name ?></td>
		<td><?= $account->serial ?></td>
		<td><?= $account->balance ?></td>
		<td><a href="/accounts/transfers/<?= $account->serial ?>">Посмотреть переводы</a></td>
	</tr>
	<?php } ?>
</table>