<html>
	<head>
		<title>Задание #5</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="/css/style.css">
	</head>
	<body>

		<ul class="nav nav-pills">
		  <li>
		  	<a href="/createAccount">Создать счёт</a>
		  </li>
		  <li>
		  	<a href="/transfer">Провести перевод</a>
		  </li>
		  <li>
		  	<a href="/accounts">Список счетов</a>
		  </li>
		</ul>
		
		<div id="app_alert" class="alert" style="display: none"></div>

		<?php include "$page.php" ?>

	</body>
	<script type="text/javascript" src="js/lib/jquery.js"></script>
	<script type="text/javascript" src="js/lib/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/lib/bootstrap-select.js"></script>
	<script type="text/javascript" src="js/app.js"></script>
	<script type="text/javascript" src="js/<?= $page?>.js"></script>
</html>