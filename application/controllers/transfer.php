<?php

class transfer extends CI_Controller
{
	function index() 
	{
		
		$this->load->model('Account');
		$accounts = $this->Account->getAccounts();
		$this->load->view('layot',  array(
			'page' => 'transfer', 
			'accounts' => $accounts
		));
	}
}