<?php

class api extends CI_Controller
{
	const HTTP_STATUS_OK = 200;
	const HTTP_STATUS_BAD_REQUEST = 400;
	const HTTP_STATUS_CONFLICT = 409;

	function addAccount() 
	{
		$status = self::HTTP_STATUS_OK;
		$message = "Добавление прошло успешно";

		$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
		$serial = filter_input(INPUT_POST, 'serial', FILTER_SANITIZE_STRING);
		$balance = filter_input(INPUT_POST, 'balance', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

		if (!strlen($name) || !strlen($serial) || !strlen($balance)) {
			$status = self::HTTP_STATUS_BAD_REQUEST;
			$message = "Неверно заполнена форма";
		} else {
			$account = array(
				'name' => $name,
				'serial' => $serial,
				'balance' => $balance
			);

			$this->load->model('Account');
			$this->Account->addAccount($account);

			if ($this->Account->wasSerialConflict()) {
				$status = self::HTTP_STATUS_CONFLICT;
				$message = "Пользователь с таким серийным номером уже существует";
			}

		}

		$this->_apiResponse($status, $message);
	}

	function makeTransfer()
	{
		$status = self::HTTP_STATUS_OK;
		$message = "Перевод успешно проведён";

		$sender = filter_input(INPUT_POST, 'sender', FILTER_SANITIZE_STRING);
		$recepient = filter_input(INPUT_POST, 'recepient', FILTER_SANITIZE_STRING);
		$sum = filter_input(INPUT_POST, 'sum', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

		if (!strlen($recepient) || !strlen($sender) || !strlen($sum) || $sender === $recepient) {
			$status = self::HTTP_STATUS_BAD_REQUEST;
			$message = "Неверно заполнена форма";
		} else {

			$this->load->model('Transfer');
			if (!$this->Transfer->makeTransfer($sender, $recepient, $sum)) {
				$message = "На счету не хватает средств";
				$status = self::HTTP_STATUS_CONFLICT;
			}
		}

		$this->_apiResponse($status, $message);
	}

	private function _apiResponse($status, $message)
	{
		echo $message;
		$this->output->set_status_header($status);
	}
}