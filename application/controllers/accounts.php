<?php

class accounts extends CI_Controller
{
	function index() 
	{
		$this->load->model('Account');
		$accounts = $this->Account->getAccounts();

		$this->load->view('layot',  array(
			'page' => 'accounts', 
			'accounts' => $accounts
		));
	}

	function transfers($serial)
	{	
		$this->load->model('Transfer');
		$transfers = $this->Transfer->getTransfersBySenderSerial($serial);

		$this->load->view('layot',  array(
			'page' => 'transfers', 
			'transfers' => $transfers,
			'serial' => $serial
		));
	}
}